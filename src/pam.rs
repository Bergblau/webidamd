/* Copyright 2021 Dominik George <dominik.george@teckids.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

use crate::config::{
    argv_to_config,
    get_config,
    get_optional,
};
use config::Config;

use crate::oauth::{get_access_token_password, get_data_jq, get_usable_token};

use crate::logging::setup_log;

use crate::cache::{get_context_user, set_is_getpwnam_safe};

use crate::unix::getpwnam_safe;

use pamsm::{PamServiceModule, Pam, PamFlag, PamError, PamLibExt};

fn pam_sm_prepare(argv: &Vec<String>) -> Config {
    let conf_args = argv_to_config(argv);
    let conf = get_config(Some(conf_args));

    let mut log_level = log::LevelFilter::Error;
    if get_optional(&conf, "pam.debug").unwrap_or_default() {
        log_level = log::LevelFilter::Debug;
    }
    setup_log(log_level);

    return conf;
}

struct PamOidc;

impl PamServiceModule for PamOidc {
    fn authenticate(pamh: Pam, _: PamFlag, argv: Vec<String>) -> PamError {
        let conf = pam_sm_prepare(&argv);
        debug!("[PAM] authenticate called");

        if conf.get_str("pam.flow").unwrap() == "password" {
            debug!("Starting Resource Owner Password Credentials OAuth flow");

            let username = match pamh.get_user(None) {
                Ok(Some(u)) => match u.to_str() {
                    Ok(u) => u,
                    _ => {
                        error!("Could not convert user name to string, aborting");
                        return PamError::CRED_INSUFFICIENT;
                    },
                },
                Ok(None) => {
                    error!("No username supplied");
                    return PamError::CRED_INSUFFICIENT;
                },
                Err(e) => {
                    error!("Error getting user name: {}", e);
                    return e;
                },
            };
            debug!("Successfully got user name");

            let password = match pamh.get_authtok(None) {
                Ok(Some(p)) => match p.to_str() {
                    Ok(p) => p,
                    _ => {
                        error!("Could not convert password to string");
                        return PamError::CRED_INSUFFICIENT;
                    },
                },
                Ok(None) => {
                    error!("No password supplied");
                    return PamError::CRED_INSUFFICIENT;
                },
                Err(e) => {
                    error!("Error getting password: {}", e);
                    return e;
                },
            };
            debug!("Successfully got password");

            match get_access_token_password(&conf, "pam", username.to_string(), password.to_string(), PamError::SERVICE_ERR, PamError::AUTH_ERR) {
                Ok(t) => {
                    info!("Authenticated {} using Resource Owner Password Grant", username);

                    // Cast a a butt-backwards somersault to get the token where it belongs
                    // This is because we cannot store the token in the user's home directory
                    // without resolving the user through getpwnam, and we (probably) cannot
                    // do getpwnam without having the user's access token, and we (certainly)
                    // cannot do getpwnam form inside the UserInfo cache object while having
                    // the UserInfo cache object locked for PAM. Therefore...

                    // 1. ...mark getpwnam unsafe (prevent cache code from calling it)
                    set_is_getpwnam_safe(false);
                    // 2. ...store the access token in memory, so the NSS part can use it
                    //    on re-entry through getpwnam
                    get_context_user().set_access_token(t.clone(), false, false).ok();
                    // 3. ...call getpwnam ourselves without having the cache object locked
                    let passwd = getpwnam_safe(username.to_string());
                    if passwd.is_ok() {
                        // 4. ...if getpwnam was successful, store the result for context user;
                        //    setcred() will pick it up and write to /run and/or $HOME
                        get_context_user().set_passwd(passwd.unwrap());
                    }
                    // 5. ...unlock getpwnam again (somewhat unnecessary)
                    set_is_getpwnam_safe(true);
                    return PamError::SUCCESS;
                },
                Err(e) => {
                    error!("Error in legacy authentication: {}", e);
                    return e;
                },
            };
        }

        error!("Unknown flow for authentication");
        return PamError::SERVICE_ERR;
    }

    fn acct_mgmt(pamh: Pam, _: PamFlag, argv: Vec<String>) -> PamError {
        let conf = pam_sm_prepare(&argv);
        debug!("[PAM] acct_mgmt called");

        if conf.get_str("pam.urls.authz").unwrap_or_default() == "" {
            info!("Authorization endpoint not set, granting access by default");
            return PamError::SUCCESS;
        }

        debug!("Checking authorization with server");

        // Retrieve access token (as acquired in the auth stage, probably)
        set_is_getpwnam_safe(false);
        let token = match get_usable_token(&conf, "pam", &mut get_context_user()) {
            Some(t) => t,
            None => {
                error!("Failed to get usable access token");
                return PamError::CRED_UNAVAIL;
            }
        };
        set_is_getpwnam_safe(true);

        // Get and transform data from API
        let data: bool = match get_data_jq(&conf, "pam", "authz", "".to_string(), &token, true) {
            Ok(d) => d,
            Err(e) => {
                error!("Could not load JSON data for authorization: {}", e);
                return PamError::SERVICE_ERR;
            }
        };

        match data {
            true => {
                info!("Authorization granted");
                PamError::SUCCESS
            },
            false => {
                info!("Authorization denied");
                PamError::PERM_DENIED
            }
        }
    }

    fn setcred(pamh: Pam, flag: PamFlag, argv: Vec<String>) -> PamError {
        let conf = pam_sm_prepare(&argv);
        debug!("[PAM] setcred called");

        let persist_run = conf.get_bool("pam.persist_token.run").unwrap();
        let persist_home = conf.get_bool("pam.persist_token.home").unwrap();

        let token = match get_context_user().get_access_token() {
            Some(t) => t,
            None => {
                error!("Access token not yet known, cannot persist");
                return PamError::CRED_UNAVAIL;
            }
        };

        let res;
        if matches!(flag, PamFlag::ESTABLISH_CRED) {
            set_is_getpwnam_safe(false);
            let token =
            res = match get_context_user().set_access_token(token.clone(), persist_run, persist_home) {
                Ok(_) => {
                    debug!("Successfully set credentials");
                    PamError::SUCCESS
                },
                Err(e) => {
                    error!("Failed setting credentials: {}", e);
                    PamError::CRED_ERR
                }
            };
            set_is_getpwnam_safe(true);
        } else if matches!(flag, PamFlag::DELETE_CRED) {
            // FIXME Implement in UserInfo class
            res = PamError::SERVICE_ERR;
        } else if matches!(flag, PamFlag::REINITIALIZE_CRED) {
            // FIXME Implement
            res = PamError::SERVICE_ERR;
        } else if matches!(flag, PamFlag::REFRESH_CRED) {
            // FIXME Implement in UserInfo class (probably)
            res = PamError::SERVICE_ERR;
        } else {
            error!("setcred called with unexpected flag");
            res = PamError::SERVICE_ERR;
        }

        res
    }
}

pam_module!(PamOidc);
