# Example configuration for system-auth-webapi
#
# This configuration uses almost all features of the library.
# It is tailored to the backend implemented in AlekSIS-App-NIS,
# which is the de facto reference implementation.

# Enable debug globally (NSS and PAM)
# This may leak sensitive information to syslog!
debug = true

# OAuth endpoints to acquire tokens, used globally (NSS and PAM)
auth_url = "https://ticdesk-dev.teckids.org/oauth/authorize/"
token_url = "https://ticdesk-dev.teckids.org/oauth/token/"

# Configuration for the PAM component
[pam]
# Client ID and secret for acquiring OAuth tokens
# You might want to put these into a separate file system_auth_webapi.secret.toml!
client_id = "Df1cpPEBsbG64oZ1Q1L8NetH1UKNBUyA5qhxg1Zh"
client_secret = ""

# Persist the user token in the filesystem
# Possible values:
#  run - Only persist to runtime directory (probably /run)
#  home - Also persist to home directory
# If NSS is in use, the token MUST be persisted to at least /run, or user-only
# mode will not work.
# Defaults to only /run
persist_token = { run = true, home = true }

# Endpoint URL to retrieve to check current authorization to use the system
#
# If set, the mapping program below must convert the response to a bool
# If unset, authorization is always granted
#  urls.authz = ""

# Mapping program (using jq) that converts the response from the authorization
# endpoint to a boolean
#  maps.authz = "."

[nss]
# Client ID and secret for acquiring OAuth tokens
# You might want to put these into a separate file system_auth_webapi.secret.toml!
client_id = "z8Oz0tG56QRo9QEPUZTs5Eda410FMiJtYxlInxKE"
client_secret = ""

# Endpoint URLs for retrieving information for NSS databases
# For single-object URLs, the placeholder `{}` will be replaced with the lookup key
#  list - retrieve a list of users, e.g. `getent passwd`
urls.passwd.list = "https://ticdesk-dev.teckids.org/app/nis/api/passwd/"
#  by_uid - get informatio non one user by their numeric ID, e.g. getent passwd 1234
urls.passwd.by_uid = "https://ticdesk-dev.teckids.org/app/nis/api/passwd/{}/"
#  by_name - get informatio non one user by their login name, e.g. getent passwd jdoe
urls.passwd.by_name = "https://ticdesk-dev.teckids.org/app/nis/api/passwd/{}/"

# The following configuration maps the attributes as returned by AlekSIS, as
# example onto a system that also has local accounts (thus mapping IDs and
# home directories).
# The map is a jq program that gets one user object from the API backend as
# input and outputs an object with the expected keys for the passwd struct.
maps.passwd = """
    {
        name: .username,
        # No passwords in passwd
        passwd: "x",
        # Map user and group IDs starting at 10000
        uid: (.uid + 10000),
        gid: (.primary_gid + 10000),
        # Append organisation name to Gecos field
        gecos: (.full_name + " (Teckids)"),
        # Remap /home from server to /srv/teckids locally
        dir: ("/srv/teckids/" + (.home_directory|ltrimstr("/home/"))),
        shell: .login_shell
    }
"""

# Reverse mapping to make sure uid lookups on entries mapped above still work
# Will be applied with jq to the lookup key before inserting into the endpoint URL
maps.rev.passwd.by_uid = ". - 10000"
